## Problem description

The standard way to detect whether NodeJS module is called directly (via CLI) or is required by another module is to use
```module === require.main```. 
When using Webpack both that objects do not refer to the original objects ```module``` and ```require.main``` provided by NodeJS, as we expect by default.
See this [issue](https://github.com/webpack/webpack/issues/20#issuecomment-323500902)

---

## How to use

1. ```npm i --save is-main-module```
2. Make sure you have set ```externals: [nodeExternals()]``` in your Webpack config.
3. Check if your module is required by another one with ```!isMain()```. In addition you can get refs to this ```module``` and ```require.main```

---

## Example

```
const thisModule = require('is-main-module');

const isThisModuleRequired = thisModule.!isMain();
const isThisModuleMain = thisModule.isMain();
const refToThisModule = thisModule.moduleRef;
const refToMainModule = thisModule.mainRef;
```


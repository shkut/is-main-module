const moduleRef = module.parent;
const mainRef = require.main;

module.exports = {
    moduleRef,
    mainRef,
    isMain: () => moduleRef === mainRef
}